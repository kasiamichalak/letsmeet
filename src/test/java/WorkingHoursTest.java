import org.junit.jupiter.api.*;

import java.time.LocalTime;

class WorkingHoursTest {

    private static final LocalTime CORRECT_TIME_WITHIN = LocalTime.of(11, 25);
    private static final LocalTime CORRECT_WORKING_HOURS_START = LocalTime.of(9, 00);
    private static final LocalTime CORRECT_WORKING_HOURS_END = LocalTime.of(20, 00);
    private static final WorkingHours CORRECT_WORKING_HOURS_OBJECT = new WorkingHours
            (LocalTime.of(9, 00), LocalTime.of(20, 00));

    private static final LocalTime FAIL_TIME_WITHIN = LocalTime.of(7, 04);
    private static final LocalTime FAIL_WORKING_HOURS_START = LocalTime.of(13, 00);
    private static final LocalTime FAIL_WORKING_HOURS_END = LocalTime.of(12, 00);
    private static final LocalTime FAIL_WORKING_HOURS_START_NULL = null;
    private static final LocalTime FAIL_WORKING_HOURS_END_NULL = null;

    @Test
    void shouldCreateWorkingHoursObjectIfCreatedWithStartEarlierThanEnd() {
        WorkingHours workingHours = new WorkingHours
                (CORRECT_WORKING_HOURS_START, CORRECT_WORKING_HOURS_END);
        Assertions.assertEquals(CORRECT_WORKING_HOURS_START, workingHours.getStart());
        Assertions.assertEquals(CORRECT_WORKING_HOURS_END, workingHours.getEnd());
    }

    @Test
    void shouldThrowNoExceptionIfWorkingHoursObjectCreatedWithStartEarlierThanEnd() {
        Assertions.assertDoesNotThrow(
                () -> new WorkingHours(CORRECT_WORKING_HOURS_START, CORRECT_WORKING_HOURS_END));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionIfWorkingHoursObjectCreatedWithStartLaterThanEnd() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new WorkingHours(FAIL_WORKING_HOURS_START, FAIL_WORKING_HOURS_END));
    }

    @Test
    void shouldThrowNullPointerExceptionIfWorkingHoursObjectCreatedWithNullStartOrEnd() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new WorkingHours(FAIL_WORKING_HOURS_START_NULL, CORRECT_WORKING_HOURS_END));
        Assertions.assertThrows(NullPointerException.class,
                () -> new WorkingHours(CORRECT_WORKING_HOURS_START, FAIL_WORKING_HOURS_END_NULL));
    }

    @Test
    void shouldReturnTrueIfGivenTimeIsWithinGivenWorkingHours() {
        Assertions.assertTrue(CORRECT_WORKING_HOURS_OBJECT.isWithin(CORRECT_TIME_WITHIN));
    }

    @Test
    void shouldReturnFalseIfGivenTimeIsNotWithinGivenWorkingHours() {
        Assertions.assertFalse(CORRECT_WORKING_HOURS_OBJECT.isWithin(FAIL_TIME_WITHIN));
    }
}