import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

class CalendarTest {

    private static final Calendar CORRECT_CALENDAR_1_OBJECT = new Calendar
            (new WorkingHours(LocalTime.of(9, 00), LocalTime.of(20, 00)),
                    Arrays.asList(new Meeting(LocalTime.of(9, 00), LocalTime.of(10, 30)),
                            new Meeting(LocalTime.of(12, 00), LocalTime.of(13, 00)),
                            new Meeting(LocalTime.of(16, 00), LocalTime.of(18, 30))));

    private static final Calendar CORRECT_CALENDAR_2_OBJECT = new Calendar
            (new WorkingHours(LocalTime.of(10, 00), LocalTime.of(18, 30)),
                    Arrays.asList(new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 30)),
                            new Meeting(LocalTime.of(12, 30), LocalTime.of(14, 30)),
                            new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                            new Meeting(LocalTime.of(16, 00), LocalTime.of(17, 00))));

    private static final Calendar CORRECT_CALENDAR_3_OBJECT = new Calendar
            (new WorkingHours(LocalTime.of(8, 30), LocalTime.of(15, 30)),
                    Arrays.asList(new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 00)),
                            new Meeting(LocalTime.of(12, 00), LocalTime.of(12, 30)),
                            new Meeting(LocalTime.of(13, 30), LocalTime.of(14, 00)),
                            new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                            new Meeting(LocalTime.of(15, 00), LocalTime.of(15, 30))));

    private static final List<Meeting> CORRECT_COMMON_PLANNED_MEETINGS_LIST_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_2_OBJECT = Arrays.asList
            (new Meeting(LocalTime.of(9, 00), LocalTime.of(10, 30)),
                    new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 30)),
                    new Meeting(LocalTime.of(12, 00), LocalTime.of(13, 00)),
                    new Meeting(LocalTime.of(12, 30), LocalTime.of(14, 30)),
                    new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                    new Meeting(LocalTime.of(16, 00), LocalTime.of(17, 00)),
                    new Meeting(LocalTime.of(16, 00), LocalTime.of(18, 30)));

    private static final List<Meeting> CORRECT_COMMON_PLANNED_MEETINGS_LIST_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_3_OBJECT = Arrays.asList
            (new Meeting(LocalTime.of(9, 00), LocalTime.of(10, 30)),
                    new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 00)),
                    new Meeting(LocalTime.of(12, 00), LocalTime.of(12, 30)),
                    new Meeting(LocalTime.of(12, 00), LocalTime.of(13, 00)),
                    new Meeting(LocalTime.of(13, 30), LocalTime.of(14, 00)),
                    new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                    new Meeting(LocalTime.of(15, 00), LocalTime.of(15, 30)),
                    new Meeting(LocalTime.of(16, 00), LocalTime.of(18, 30)));

    private static final WorkingHours CORRECT_COMMON_WORKING_HOURS_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_2_OBJECT = new WorkingHours
            (LocalTime.of(10, 00), LocalTime.of(18, 30));

    private static final WorkingHours CORRECT_COMMON_WORKING_HOURS_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_3_OBJECT = new WorkingHours
            (LocalTime.of(9, 00), LocalTime.of(15, 30));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_1_OBJECT = Arrays.asList
            (new TimeGap(LocalTime.of(10, 30), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(13, 00), LocalTime.of(16, 00)),
                    new TimeGap(LocalTime.of(18, 30), LocalTime.of(20, 00)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_2_OBJECT = Arrays.asList
            (new TimeGap(LocalTime.of(11, 30), LocalTime.of(12, 30)),
                    new TimeGap(LocalTime.of(15, 00), LocalTime.of(16, 00)),
                    new TimeGap(LocalTime.of(17, 00), LocalTime.of(18, 30)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_3_OBJECT = Arrays.asList
            (new TimeGap(LocalTime.of(8, 30), LocalTime.of(10, 00)),
                    new TimeGap(LocalTime.of(11, 00), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(12, 30), LocalTime.of(13, 30)),
                    new TimeGap(LocalTime.of(14, 00), LocalTime.of(14, 30)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_2_OBJECTS = Arrays.asList
            (new TimeGap(LocalTime.of(11, 30), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(15, 00), LocalTime.of(16, 00)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_3_OBJECTS = Arrays.asList
            (new TimeGap(LocalTime.of(11, 00), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(13, 00), LocalTime.of(13, 30)),
                    new TimeGap(LocalTime.of(14, 00), LocalTime.of(14, 40)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_2_OBJECTS_AVAILABLE_FOR_CORRECT_MEETING_DURATION = Arrays.asList
            (new TimeGap(LocalTime.of(11, 30), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(15, 00), LocalTime.of(16, 00)));

    private static final List<TimeGap> CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_3_OBJECTS_AVAILABLE_FOR_CORRECT_MEETING_DURATION = Arrays.asList
            (new TimeGap(LocalTime.of(11, 00), LocalTime.of(12, 00)),
                    new TimeGap(LocalTime.of(13, 00), LocalTime.of(13, 30)),
                    new TimeGap(LocalTime.of(14, 00), LocalTime.of(14, 30)));

    private static final Duration CORRECT_MEETING_DURATION = Duration.ofMinutes(30);

    @Test
    void shouldReturnCorrectCommonPlannedMeetingsForTwoGivenCalendars() {
        Assertions.assertIterableEquals(CORRECT_COMMON_PLANNED_MEETINGS_LIST_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_2_OBJECT,
                CORRECT_CALENDAR_1_OBJECT.getCommonPlannedMeetings(CORRECT_CALENDAR_2_OBJECT));
        Assertions.assertIterableEquals(CORRECT_COMMON_PLANNED_MEETINGS_LIST_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_3_OBJECT,
                CORRECT_CALENDAR_1_OBJECT.getCommonPlannedMeetings(CORRECT_CALENDAR_3_OBJECT));
    }

    @Test
    void shouldReturnCorrectCommonWorkingHoursForTwoGivenCalendars() {
        Assertions.assertEquals(CORRECT_COMMON_WORKING_HOURS_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_2_OBJECT,
                CORRECT_CALENDAR_1_OBJECT.getCommonWorkingHours(CORRECT_CALENDAR_2_OBJECT));
        Assertions.assertEquals(CORRECT_COMMON_WORKING_HOURS_FOR_CALENDAR_1_OBJECT_AND_CALENDAR_3_OBJECT,
                CORRECT_CALENDAR_1_OBJECT.getCommonWorkingHours(CORRECT_CALENDAR_3_OBJECT));
    }

    @Test
    void shouldReturnCorrectTimeGapsBetweenGivenPlannedMeetingsInGivenCalendar() {
        Assertions.assertIterableEquals(CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_1_OBJECT,
                CORRECT_CALENDAR_1_OBJECT.getTimeGaps(CORRECT_CALENDAR_1_OBJECT.getPlannedMeetings(),
                        CORRECT_CALENDAR_1_OBJECT.getWorkingHours()));
        Assertions.assertIterableEquals(CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_2_OBJECT,
                CORRECT_CALENDAR_2_OBJECT.getTimeGaps(CORRECT_CALENDAR_2_OBJECT.getPlannedMeetings(),
                        CORRECT_CALENDAR_2_OBJECT.getWorkingHours()));
        Assertions.assertIterableEquals(CORRECT_TIME_GAPS_LIST_FOR_CALENDAR_3_OBJECT,
                CORRECT_CALENDAR_3_OBJECT.getTimeGaps(CORRECT_CALENDAR_3_OBJECT.getPlannedMeetings(),
                        CORRECT_CALENDAR_3_OBJECT.getWorkingHours()));
    }

    @Test
    void shouldReturnCorrectTimeGapsCommonForTwoCalendarsAvailableForGivenMeetingDuration() {
        Assertions.assertIterableEquals(CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_2_OBJECTS_AVAILABLE_FOR_CORRECT_MEETING_DURATION,
                CORRECT_CALENDAR_1_OBJECT.getTimeGapsAvailableForMeeting(CORRECT_CALENDAR_2_OBJECT, CORRECT_MEETING_DURATION));
        Assertions.assertIterableEquals(CORRECT_TIME_GAPS_LIST_COMMON_FOR_CALENDAR_1_AND_CALENDAR_3_OBJECTS_AVAILABLE_FOR_CORRECT_MEETING_DURATION,
                CORRECT_CALENDAR_1_OBJECT.getTimeGapsAvailableForMeeting(CORRECT_CALENDAR_3_OBJECT, CORRECT_MEETING_DURATION));
    }
}