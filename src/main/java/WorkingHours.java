import java.time.LocalTime;

public class WorkingHours {

    private LocalTime start;
    private LocalTime end;
    private boolean isWithin;

    public WorkingHours(LocalTime start, LocalTime end) throws IllegalArgumentException, NullPointerException {
        if (!start.isBefore(end)) throw new IllegalArgumentException(
                "Start time should be earlier than end time of working hours.");
        if (start == null || end == null) throw new NullPointerException(
                "Argument cannot be null.");
            this.start = start;
            this.end = end;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public boolean isWithin(LocalTime time) {
        return !time.isBefore(start) & !time.isAfter(end) ? true : false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkingHours that = (WorkingHours) o;

        if (isWithin != that.isWithin) return false;
        if (!start.equals(that.start)) return false;
        return end.equals(that.end);
    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + (isWithin ? 1 : 0);
        return result;
    }
}
