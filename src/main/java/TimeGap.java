import java.time.LocalTime;

public class TimeGap {

    private LocalTime start;
    private LocalTime end;

    public TimeGap(LocalTime start, LocalTime end) throws IllegalArgumentException, NullPointerException {
        if (!start.isBefore(end)) throw new IllegalArgumentException(
                "Start time should be earlier than end time of a time gap.");
        if (start == null || end == null) throw new NullPointerException(
                "Argument cannot be null.");
        this.start = start;
        this.end = end;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeGap timeGap = (TimeGap) o;

        if (start != null ? !start.equals(timeGap.start) : timeGap.start != null) return false;
        return end != null ? end.equals(timeGap.end) : timeGap.end == null;
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }
}
