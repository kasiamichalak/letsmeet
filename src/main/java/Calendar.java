import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Calendar {

    private WorkingHours workingHours;
    private List<Meeting> plannedMeetings;
    private List<TimeGap> timeGaps;

    public Calendar(WorkingHours workingHours, List<Meeting> plannedMeetings) {
        this.workingHours = workingHours;
        this.plannedMeetings = plannedMeetings;
    }

    public WorkingHours getWorkingHours() {
        return workingHours;
    }

    public List<Meeting> getPlannedMeetings() {
        return plannedMeetings;
    }

    List<Meeting> getCommonPlannedMeetings(Calendar otherCalendar) {
        List<Meeting> commonPlannedMeetings = new ArrayList<>(plannedMeetings);
        commonPlannedMeetings.addAll(otherCalendar.getPlannedMeetings());
        sortMeetingsChronologically(commonPlannedMeetings);
        return commonPlannedMeetings;
    }

    WorkingHours getCommonWorkingHours(Calendar otherCalendar) {
        LocalTime start = workingHours.getStart().isAfter(otherCalendar.workingHours.getStart()) ? workingHours.getStart() : otherCalendar.workingHours.getStart();
        LocalTime end = workingHours.getEnd().isBefore(otherCalendar.workingHours.getEnd()) ? workingHours.getEnd() : otherCalendar.workingHours.getEnd();
        WorkingHours commonWorkingHours = new WorkingHours(start, end);
        return commonWorkingHours;
    }

    private void sortMeetingsChronologically(List<Meeting> meetings) {
        meetings.sort(Comparator.comparing(Meeting::getStart));
        for (int i = 0, j = 1; j < meetings.size(); i++, j++) {
            if (meetings.get(i).getStart().equals(meetings.get(j).getStart()) &
                    meetings.get(i).getEnd().isAfter(meetings.get(j).getEnd())) {
                meetings.sort(Comparator.comparing(Meeting::getEnd));
            }
        }
    }

    private void sortTimeGapsChronologically(List<TimeGap> timeGaps) {
        timeGaps.sort(Comparator.comparing(TimeGap::getStart));
    }

    public List<TimeGap> getTimeGaps(List<Meeting> plannedMeetings, WorkingHours workingHours) {

        timeGaps = new ArrayList<>();

        LocalTime timeGapStart = workingHours.getStart();
        LocalTime timeGapEnd = plannedMeetings.stream()
                .min(Comparator.comparing(Meeting::getStart))
                .get().getStart();
        try {
            timeGaps.add(new TimeGap(timeGapStart, timeGapEnd));
        } catch (IllegalArgumentException e) { }
        catch (NullPointerException e) { }

        timeGapStart = plannedMeetings.stream()
                .max(Comparator.comparing(Meeting::getEnd))
                .get().getEnd();
        timeGapEnd = workingHours.getEnd();
        try {
            timeGaps.add(new TimeGap(timeGapStart, timeGapEnd));
        } catch (IllegalArgumentException e) { }
        catch (NullPointerException e) { }

        for (int i = 0, j = 1; j < plannedMeetings.size(); i++, j++) {
            if (plannedMeetings.get(i).getEnd().isBefore(plannedMeetings.get(j).getStart())) {
                timeGapStart = plannedMeetings.get(i).getEnd();
                timeGapEnd = plannedMeetings.get(j).getStart();
                try {
                    timeGaps.add(new TimeGap(timeGapStart, timeGapEnd));
                } catch (IllegalArgumentException e) { }
                catch (NullPointerException e) { }
            }
        }
        timeGaps = timeGaps.stream()
                .filter(timeGap ->
                        workingHours.isWithin(timeGap.getStart()) &
                                workingHours.isWithin(timeGap.getEnd()))
                .sorted(Comparator.comparing(TimeGap::getStart))
                .collect(Collectors.toList());

        return timeGaps;
    }

    public List<TimeGap> getTimeGapsAvailableForMeeting(Calendar otherCalendar, Duration meetingDuration) {
        List<Meeting> plannedMeetingsInBothCalendars = getCommonPlannedMeetings(otherCalendar);
        sortMeetingsChronologically(plannedMeetingsInBothCalendars);
        WorkingHours commonWorkingHours = getCommonWorkingHours(otherCalendar);
        List<TimeGap> commonTimeGaps = getTimeGaps(plannedMeetingsInBothCalendars, commonWorkingHours);
        List<TimeGap> availableTimeGaps = commonTimeGaps.stream().filter(timeGap -> (Duration.between(timeGap.getStart(), timeGap.getEnd()).compareTo(meetingDuration)) >= 0)
                .collect(Collectors.toList());
        return availableTimeGaps;
    }
}
