import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.bind.config.PropertyOrderStrategy;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Calendar calendar1 = new Calendar(new WorkingHours(LocalTime.of(9, 00), LocalTime.of(20, 00)),
                Arrays.asList(new Meeting(LocalTime.of(9, 00), LocalTime.of(10, 30)),
                        new Meeting(LocalTime.of(12, 00), LocalTime.of(13, 00)),
                        new Meeting(LocalTime.of(16, 00), LocalTime.of(18, 30))));

        Calendar calendar2 = new Calendar(new WorkingHours(LocalTime.of(10, 00), LocalTime.of(18, 30)),
                Arrays.asList(new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 30)),
                        new Meeting(LocalTime.of(12, 30), LocalTime.of(14, 30)),
                        new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                        new Meeting(LocalTime.of(16, 00), LocalTime.of(17, 00))));

        Calendar calendar3 = new Calendar
                (new WorkingHours(LocalTime.of(8, 30), LocalTime.of(15, 30)),
                        Arrays.asList(new Meeting(LocalTime.of(10, 00), LocalTime.of(11, 00)),
                                new Meeting(LocalTime.of(12, 00), LocalTime.of(12, 30)),
                                new Meeting(LocalTime.of(13, 30), LocalTime.of(14, 00)),
                                new Meeting(LocalTime.of(14, 30), LocalTime.of(15, 00)),
                                new Meeting(LocalTime.of(15, 00), LocalTime.of(15, 30))));

        Duration meetingDuration = Duration.ofMinutes(30);

        JsonbConfig config = new JsonbConfig();
        config.withPropertyOrderStrategy(PropertyOrderStrategy.REVERSE);
        config.withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES);
        config.withDateFormat("HH:mm", null);
        Jsonb jsonb = JsonbBuilder.create(config);

        System.out.println("Calendar 1");
        System.out.println(jsonb.toJson(calendar1));

        System.out.println("Calendar 2");
        System.out.println(jsonb.toJson(calendar2));

        System.out.println("Calendar 3");
        System.out.println(jsonb.toJson(calendar3));

        System.out.println("___________________________________________________");
        System.out.printf("Proposed meeting duration: %d minutes.\n", meetingDuration.toMinutesPart());

        System.out.println("___________________________________________________");
        System.out.println("Common working hours for Calendar 1 and Calendar 2");
        WorkingHours commonWorkingHours12 = calendar1.getCommonWorkingHours(calendar2);
        System.out.println(jsonb.toJson(commonWorkingHours12));

        System.out.println("___________________________________________________");
        System.out.println("Common planned meetings for Calendar 1 and Calendar 2");
        List<Meeting> commonPlannedMeetings12 = calendar1.getCommonPlannedMeetings(calendar2);
        System.out.println(jsonb.toJson(commonPlannedMeetings12));

        System.out.println("___________________________________________________");
        System.out.println("Common time gaps for Calendar 1 and Calendar 2");
        List<TimeGap> commonTimeGaps12 = calendar1.getTimeGaps(commonPlannedMeetings12, commonWorkingHours12);
        System.out.println(jsonb.toJson(commonTimeGaps12));

        System.out.println("___________________________________________________");
        System.out.println("For Calendar 1 & Calendar 2: Proposed meeting can be arranged within the following time gaps: ");
        System.out.println(jsonb.toJson(calendar1.getTimeGaps(commonPlannedMeetings12, commonWorkingHours12)));

        System.out.println("___________________________________________________");
        System.out.println("Common working hours for Calendar 1 and Calendar 3");
        WorkingHours commonWorkingHours13 = calendar1.getCommonWorkingHours(calendar3);
        System.out.println(jsonb.toJson(commonWorkingHours13));

        System.out.println("___________________________________________________");
        System.out.println("Common planned meetings for Calendar 1 and Calendar 3");
        List<Meeting> commonPlannedMeetings13 = calendar1.getCommonPlannedMeetings(calendar3);
        System.out.println(jsonb.toJson(commonPlannedMeetings13));

        System.out.println("___________________________________________________");
        System.out.println("Common time gaps for Calendar 1 and Calendar 3");
        List<TimeGap> commonTimeGaps13 = calendar1.getTimeGaps(commonPlannedMeetings13, commonWorkingHours13);
        System.out.println(jsonb.toJson(commonTimeGaps13));

        System.out.println("___________________________________________________");
        System.out.println("For Calendar 1 & Calendar 3: Proposed meeting can be arranged within the following time gaps: ");
        System.out.println(jsonb.toJson(calendar1.getTimeGaps(commonPlannedMeetings13, commonWorkingHours13)));
    }
}
