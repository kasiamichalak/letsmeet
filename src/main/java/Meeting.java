import java.time.Duration;
import java.time.LocalTime;

public class Meeting {

    private LocalTime start;
    private LocalTime end;
    private Duration duration;

    public Meeting(LocalTime start, LocalTime end) throws IllegalArgumentException, NullPointerException {
        if (!start.isBefore(end)) throw new IllegalArgumentException(
                "Start time should be earlier than end time of a meeting.");
        if (start == null || end == null) throw new NullPointerException(
                "Argument cannot be null.");
        this.start = start;
        this.end = end;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public Duration getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meeting meeting = (Meeting) o;

        if (!start.equals(meeting.start)) return false;
        if (!end.equals(meeting.end)) return false;
        return duration != null ? duration.equals(meeting.duration) : meeting.duration == null;
    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }
}
